package info4.gl.coopcycle.service.mapper;

import info4.gl.coopcycle.domain.Commande;
import info4.gl.coopcycle.domain.Produit;
import info4.gl.coopcycle.domain.ProduitCommande;
import info4.gl.coopcycle.service.dto.CommandeDTO;
import info4.gl.coopcycle.service.dto.ProduitCommandeDTO;
import info4.gl.coopcycle.service.dto.ProduitDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link ProduitCommande} and its DTO {@link ProduitCommandeDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProduitCommandeMapper extends EntityMapper<ProduitCommandeDTO, ProduitCommande> {
    @Mapping(target = "products", source = "products", qualifiedByName = "produitIdSet")
    @Mapping(target = "commande", source = "commande", qualifiedByName = "commandeId")
    ProduitCommandeDTO toDto(ProduitCommande s);

    @Mapping(target = "removeProduct", ignore = true)
    ProduitCommande toEntity(ProduitCommandeDTO produitCommandeDTO);

    @Named("produitId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ProduitDTO toDtoProduitId(Produit produit);

    @Named("produitIdSet")
    default Set<ProduitDTO> toDtoProduitIdSet(Set<Produit> produit) {
        return produit.stream().map(this::toDtoProduitId).collect(Collectors.toSet());
    }

    @Named("commandeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CommandeDTO toDtoCommandeId(Commande commande);
}
