export enum ModeDePaiement {
  CB = 'CB',

  BITCOIN = 'BITCOIN',

  PAYPAL = 'PAYPAL',
}
