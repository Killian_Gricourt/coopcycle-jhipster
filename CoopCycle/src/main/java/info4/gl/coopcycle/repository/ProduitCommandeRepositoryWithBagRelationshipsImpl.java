package info4.gl.coopcycle.repository;

import info4.gl.coopcycle.domain.ProduitCommande;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.hibernate.annotations.QueryHints;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

/**
 * Utility repository to load bag relationships based on https://vladmihalcea.com/hibernate-multiplebagfetchexception/
 */
public class ProduitCommandeRepositoryWithBagRelationshipsImpl implements ProduitCommandeRepositoryWithBagRelationships {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Optional<ProduitCommande> fetchBagRelationships(Optional<ProduitCommande> produitCommande) {
        return produitCommande.map(this::fetchProducts);
    }

    @Override
    public Page<ProduitCommande> fetchBagRelationships(Page<ProduitCommande> produitCommandes) {
        return new PageImpl<>(
            fetchBagRelationships(produitCommandes.getContent()),
            produitCommandes.getPageable(),
            produitCommandes.getTotalElements()
        );
    }

    @Override
    public List<ProduitCommande> fetchBagRelationships(List<ProduitCommande> produitCommandes) {
        return Optional.of(produitCommandes).map(this::fetchProducts).orElse(Collections.emptyList());
    }

    ProduitCommande fetchProducts(ProduitCommande result) {
        return entityManager
            .createQuery(
                "select produitCommande from ProduitCommande produitCommande left join fetch produitCommande.products where produitCommande is :produitCommande",
                ProduitCommande.class
            )
            .setParameter("produitCommande", result)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getSingleResult();
    }

    List<ProduitCommande> fetchProducts(List<ProduitCommande> produitCommandes) {
        HashMap<Object, Integer> order = new HashMap<>();
        IntStream.range(0, produitCommandes.size()).forEach(index -> order.put(produitCommandes.get(index).getId(), index));
        List<ProduitCommande> result = entityManager
            .createQuery(
                "select distinct produitCommande from ProduitCommande produitCommande left join fetch produitCommande.products where produitCommande in :produitCommandes",
                ProduitCommande.class
            )
            .setParameter("produitCommandes", produitCommandes)
            .setHint(QueryHints.PASS_DISTINCT_THROUGH, false)
            .getResultList();
        Collections.sort(result, (o1, o2) -> Integer.compare(order.get(o1.getId()), order.get(o2.getId())));
        return result;
    }
}
