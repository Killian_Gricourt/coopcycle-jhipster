import { Component, Vue, Inject } from 'vue-property-decorator';

import { minLength, maxLength } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import CourseService from '@/entities/course/course.service';
import { ICourse } from '@/shared/model/course.model';

import { ICoursier, Coursier } from '@/shared/model/coursier.model';
import CoursierService from './coursier.service';

const validations: any = {
  coursier: {
    nom: {},
    prenom: {},
    telephone: {
      minLength: minLength(10),
      maxLength: maxLength(10),
    },
    vehicule: {},
  },
};

@Component({
  validations,
})
export default class CoursierUpdate extends Vue {
  @Inject('coursierService') private coursierService: () => CoursierService;
  @Inject('alertService') private alertService: () => AlertService;

  public coursier: ICoursier = new Coursier();

  @Inject('courseService') private courseService: () => CourseService;

  public courses: ICourse[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.coursierId) {
        vm.retrieveCoursier(to.params.coursierId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.coursier.id) {
      this.coursierService()
        .update(this.coursier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.coursier.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.coursierService()
        .create(this.coursier)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.coursier.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveCoursier(coursierId): void {
    this.coursierService()
      .find(coursierId)
      .then(res => {
        this.coursier = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.courseService()
      .retrieve()
      .then(res => {
        this.courses = res.data;
      });
  }
}
