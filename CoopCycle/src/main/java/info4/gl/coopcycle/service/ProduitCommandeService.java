package info4.gl.coopcycle.service;

import info4.gl.coopcycle.domain.ProduitCommande;
import info4.gl.coopcycle.repository.ProduitCommandeRepository;
import info4.gl.coopcycle.service.dto.ProduitCommandeDTO;
import info4.gl.coopcycle.service.mapper.ProduitCommandeMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link ProduitCommande}.
 */
@Service
@Transactional
public class ProduitCommandeService {

    private final Logger log = LoggerFactory.getLogger(ProduitCommandeService.class);

    private final ProduitCommandeRepository produitCommandeRepository;

    private final ProduitCommandeMapper produitCommandeMapper;

    public ProduitCommandeService(ProduitCommandeRepository produitCommandeRepository, ProduitCommandeMapper produitCommandeMapper) {
        this.produitCommandeRepository = produitCommandeRepository;
        this.produitCommandeMapper = produitCommandeMapper;
    }

    /**
     * Save a produitCommande.
     *
     * @param produitCommandeDTO the entity to save.
     * @return the persisted entity.
     */
    public ProduitCommandeDTO save(ProduitCommandeDTO produitCommandeDTO) {
        log.debug("Request to save ProduitCommande : {}", produitCommandeDTO);
        ProduitCommande produitCommande = produitCommandeMapper.toEntity(produitCommandeDTO);
        produitCommande = produitCommandeRepository.save(produitCommande);
        return produitCommandeMapper.toDto(produitCommande);
    }

    /**
     * Update a produitCommande.
     *
     * @param produitCommandeDTO the entity to save.
     * @return the persisted entity.
     */
    public ProduitCommandeDTO update(ProduitCommandeDTO produitCommandeDTO) {
        log.debug("Request to update ProduitCommande : {}", produitCommandeDTO);
        ProduitCommande produitCommande = produitCommandeMapper.toEntity(produitCommandeDTO);
        produitCommande = produitCommandeRepository.save(produitCommande);
        return produitCommandeMapper.toDto(produitCommande);
    }

    /**
     * Partially update a produitCommande.
     *
     * @param produitCommandeDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<ProduitCommandeDTO> partialUpdate(ProduitCommandeDTO produitCommandeDTO) {
        log.debug("Request to partially update ProduitCommande : {}", produitCommandeDTO);

        return produitCommandeRepository
            .findById(produitCommandeDTO.getId())
            .map(existingProduitCommande -> {
                produitCommandeMapper.partialUpdate(existingProduitCommande, produitCommandeDTO);

                return existingProduitCommande;
            })
            .map(produitCommandeRepository::save)
            .map(produitCommandeMapper::toDto);
    }

    /**
     * Get all the produitCommandes.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<ProduitCommandeDTO> findAll() {
        log.debug("Request to get all ProduitCommandes");
        return produitCommandeRepository
            .findAll()
            .stream()
            .map(produitCommandeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get all the produitCommandes with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    public Page<ProduitCommandeDTO> findAllWithEagerRelationships(Pageable pageable) {
        return produitCommandeRepository.findAllWithEagerRelationships(pageable).map(produitCommandeMapper::toDto);
    }

    /**
     * Get one produitCommande by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<ProduitCommandeDTO> findOne(Long id) {
        log.debug("Request to get ProduitCommande : {}", id);
        return produitCommandeRepository.findOneWithEagerRelationships(id).map(produitCommandeMapper::toDto);
    }

    /**
     * Delete the produitCommande by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete ProduitCommande : {}", id);
        produitCommandeRepository.deleteById(id);
    }
}
