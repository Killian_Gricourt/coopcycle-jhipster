package info4.gl.coopcycle.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Produit.
 */
@Entity
@Table(name = "produit")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Produit implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_produit", nullable = false, unique = true)
    private Long idProduit;

    @NotNull
    @Column(name = "id_menu", nullable = false, unique = true)
    private Long idMenu;

    @Column(name = "nom")
    private String nom;

    @NotNull
    @Column(name = "prix", nullable = false)
    private Float prix;

    @Min(value = 0)
    @Column(name = "nb_stock")
    private Integer nbStock;

    @ManyToOne
    @JsonIgnoreProperties(value = { "produits", "cooperative" }, allowSetters = true)
    private Menu menu;

    @ManyToMany(mappedBy = "products")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "products", "commande" }, allowSetters = true)
    private Set<ProduitCommande> produitcommandes = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Produit id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdProduit() {
        return this.idProduit;
    }

    public Produit idProduit(Long idProduit) {
        this.setIdProduit(idProduit);
        return this;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }

    public Long getIdMenu() {
        return this.idMenu;
    }

    public Produit idMenu(Long idMenu) {
        this.setIdMenu(idMenu);
        return this;
    }

    public void setIdMenu(Long idMenu) {
        this.idMenu = idMenu;
    }

    public String getNom() {
        return this.nom;
    }

    public Produit nom(String nom) {
        this.setNom(nom);
        return this;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Float getPrix() {
        return this.prix;
    }

    public Produit prix(Float prix) {
        this.setPrix(prix);
        return this;
    }

    public void setPrix(Float prix) {
        this.prix = prix;
    }

    public Integer getNbStock() {
        return this.nbStock;
    }

    public Produit nbStock(Integer nbStock) {
        this.setNbStock(nbStock);
        return this;
    }

    public void setNbStock(Integer nbStock) {
        this.nbStock = nbStock;
    }

    public Menu getMenu() {
        return this.menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public Produit menu(Menu menu) {
        this.setMenu(menu);
        return this;
    }

    public Set<ProduitCommande> getProduitcommandes() {
        return this.produitcommandes;
    }

    public void setProduitcommandes(Set<ProduitCommande> produitCommandes) {
        if (this.produitcommandes != null) {
            this.produitcommandes.forEach(i -> i.removeProduct(this));
        }
        if (produitCommandes != null) {
            produitCommandes.forEach(i -> i.addProduct(this));
        }
        this.produitcommandes = produitCommandes;
    }

    public Produit produitcommandes(Set<ProduitCommande> produitCommandes) {
        this.setProduitcommandes(produitCommandes);
        return this;
    }

    public Produit addProduitcommande(ProduitCommande produitCommande) {
        this.produitcommandes.add(produitCommande);
        produitCommande.getProducts().add(this);
        return this;
    }

    public Produit removeProduitcommande(ProduitCommande produitCommande) {
        this.produitcommandes.remove(produitCommande);
        produitCommande.getProducts().remove(this);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Produit)) {
            return false;
        }
        return id != null && id.equals(((Produit) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Produit{" +
            "id=" + getId() +
            ", idProduit=" + getIdProduit() +
            ", idMenu=" + getIdMenu() +
            ", nom='" + getNom() + "'" +
            ", prix=" + getPrix() +
            ", nbStock=" + getNbStock() +
            "}";
    }
}
