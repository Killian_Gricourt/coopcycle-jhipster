package info4.gl.coopcycle.service.mapper;

import info4.gl.coopcycle.domain.Commande;
import info4.gl.coopcycle.domain.Course;
import info4.gl.coopcycle.domain.Coursier;
import info4.gl.coopcycle.service.dto.CommandeDTO;
import info4.gl.coopcycle.service.dto.CourseDTO;
import info4.gl.coopcycle.service.dto.CoursierDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Course} and its DTO {@link CourseDTO}.
 */
@Mapper(componentModel = "spring")
public interface CourseMapper extends EntityMapper<CourseDTO, Course> {
    @Mapping(target = "commande", source = "commande", qualifiedByName = "commandeId")
    @Mapping(target = "coursier", source = "coursier", qualifiedByName = "coursierId")
    CourseDTO toDto(Course s);

    @Named("commandeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CommandeDTO toDtoCommandeId(Commande commande);

    @Named("coursierId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CoursierDTO toDtoCoursierId(Coursier coursier);
}
