import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minValue } from 'vuelidate/lib/validators';
import dayjs from 'dayjs';
import { DATE_TIME_LONG_FORMAT } from '@/shared/date/filters';

import AlertService from '@/shared/alert/alert.service';

import ProduitCommandeService from '@/entities/produit-commande/produit-commande.service';
import { IProduitCommande } from '@/shared/model/produit-commande.model';

import PaiementService from '@/entities/paiement/paiement.service';
import { IPaiement } from '@/shared/model/paiement.model';

import CourseService from '@/entities/course/course.service';
import { ICourse } from '@/shared/model/course.model';

import ClientService from '@/entities/client/client.service';
import { IClient } from '@/shared/model/client.model';

import CooperativeService from '@/entities/cooperative/cooperative.service';
import { ICooperative } from '@/shared/model/cooperative.model';

import { ICommande, Commande } from '@/shared/model/commande.model';
import CommandeService from './commande.service';
import { Etat } from '@/shared/model/enumerations/etat.model';

const validations: any = {
  commande: {
    idCommande: {
      required,
      numeric,
    },
    idCooperative: {
      required,
      numeric,
    },
    idClient: {
      required,
      numeric,
    },
    idCourse: {
      required,
      numeric,
    },
    prix: {
      required,
      numeric,
      min: minValue(0),
    },
    date: {},
    etat: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class CommandeUpdate extends Vue {
  @Inject('commandeService') private commandeService: () => CommandeService;
  @Inject('alertService') private alertService: () => AlertService;

  public commande: ICommande = new Commande();

  @Inject('produitCommandeService') private produitCommandeService: () => ProduitCommandeService;

  public produitCommandes: IProduitCommande[] = [];

  @Inject('paiementService') private paiementService: () => PaiementService;

  public paiements: IPaiement[] = [];

  @Inject('courseService') private courseService: () => CourseService;

  public courses: ICourse[] = [];

  @Inject('clientService') private clientService: () => ClientService;

  public clients: IClient[] = [];

  @Inject('cooperativeService') private cooperativeService: () => CooperativeService;

  public cooperatives: ICooperative[] = [];
  public etatValues: string[] = Object.keys(Etat);
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.commandeId) {
        vm.retrieveCommande(to.params.commandeId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.commande.id) {
      this.commandeService()
        .update(this.commande)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.commande.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.commandeService()
        .create(this.commande)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.commande.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public convertDateTimeFromServer(date: Date): string {
    if (date && dayjs(date).isValid()) {
      return dayjs(date).format(DATE_TIME_LONG_FORMAT);
    }
    return null;
  }

  public updateInstantField(field, event) {
    if (event.target.value) {
      this.commande[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.commande[field] = null;
    }
  }

  public updateZonedDateTimeField(field, event) {
    if (event.target.value) {
      this.commande[field] = dayjs(event.target.value, DATE_TIME_LONG_FORMAT);
    } else {
      this.commande[field] = null;
    }
  }

  public retrieveCommande(commandeId): void {
    this.commandeService()
      .find(commandeId)
      .then(res => {
        res.date = new Date(res.date);
        this.commande = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.produitCommandeService()
      .retrieve()
      .then(res => {
        this.produitCommandes = res.data;
      });
    this.paiementService()
      .retrieve()
      .then(res => {
        this.paiements = res.data;
      });
    this.courseService()
      .retrieve()
      .then(res => {
        this.courses = res.data;
      });
    this.clientService()
      .retrieve()
      .then(res => {
        this.clients = res.data;
      });
    this.cooperativeService()
      .retrieve()
      .then(res => {
        this.cooperatives = res.data;
      });
  }
}
