package info4.gl.coopcycle.service.mapper;

import info4.gl.coopcycle.domain.Menu;
import info4.gl.coopcycle.domain.Produit;
import info4.gl.coopcycle.service.dto.MenuDTO;
import info4.gl.coopcycle.service.dto.ProduitDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Produit} and its DTO {@link ProduitDTO}.
 */
@Mapper(componentModel = "spring")
public interface ProduitMapper extends EntityMapper<ProduitDTO, Produit> {
    @Mapping(target = "menu", source = "menu", qualifiedByName = "menuId")
    ProduitDTO toDto(Produit s);

    @Named("menuId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    MenuDTO toDtoMenuId(Menu menu);
}
