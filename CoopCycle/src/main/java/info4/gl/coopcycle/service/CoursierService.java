package info4.gl.coopcycle.service;

import info4.gl.coopcycle.domain.Coursier;
import info4.gl.coopcycle.repository.CoursierRepository;
import info4.gl.coopcycle.service.dto.CoursierDTO;
import info4.gl.coopcycle.service.mapper.CoursierMapper;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Coursier}.
 */
@Service
@Transactional
public class CoursierService {

    private final Logger log = LoggerFactory.getLogger(CoursierService.class);

    private final CoursierRepository coursierRepository;

    private final CoursierMapper coursierMapper;

    public CoursierService(CoursierRepository coursierRepository, CoursierMapper coursierMapper) {
        this.coursierRepository = coursierRepository;
        this.coursierMapper = coursierMapper;
    }

    /**
     * Save a coursier.
     *
     * @param coursierDTO the entity to save.
     * @return the persisted entity.
     */
    public CoursierDTO save(CoursierDTO coursierDTO) {
        log.debug("Request to save Coursier : {}", coursierDTO);
        Coursier coursier = coursierMapper.toEntity(coursierDTO);
        coursier = coursierRepository.save(coursier);
        return coursierMapper.toDto(coursier);
    }

    /**
     * Update a coursier.
     *
     * @param coursierDTO the entity to save.
     * @return the persisted entity.
     */
    public CoursierDTO update(CoursierDTO coursierDTO) {
        log.debug("Request to update Coursier : {}", coursierDTO);
        Coursier coursier = coursierMapper.toEntity(coursierDTO);
        coursier = coursierRepository.save(coursier);
        return coursierMapper.toDto(coursier);
    }

    /**
     * Partially update a coursier.
     *
     * @param coursierDTO the entity to update partially.
     * @return the persisted entity.
     */
    public Optional<CoursierDTO> partialUpdate(CoursierDTO coursierDTO) {
        log.debug("Request to partially update Coursier : {}", coursierDTO);

        return coursierRepository
            .findById(coursierDTO.getId())
            .map(existingCoursier -> {
                coursierMapper.partialUpdate(existingCoursier, coursierDTO);

                return existingCoursier;
            })
            .map(coursierRepository::save)
            .map(coursierMapper::toDto);
    }

    /**
     * Get all the coursiers.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<CoursierDTO> findAll() {
        log.debug("Request to get all Coursiers");
        return coursierRepository.findAll().stream().map(coursierMapper::toDto).collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one coursier by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<CoursierDTO> findOne(Long id) {
        log.debug("Request to get Coursier : {}", id);
        return coursierRepository.findById(id).map(coursierMapper::toDto);
    }

    /**
     * Delete the coursier by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Coursier : {}", id);
        coursierRepository.deleteById(id);
    }
}
