import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import CommandeService from '@/entities/commande/commande.service';
import { ICommande } from '@/shared/model/commande.model';

import CoursierService from '@/entities/coursier/coursier.service';
import { ICoursier } from '@/shared/model/coursier.model';

import { ICourse, Course } from '@/shared/model/course.model';
import CourseService from './course.service';

const validations: any = {
  course: {
    idCoursier: {
      required,
      numeric,
    },
    idCommande: {
      required,
      numeric,
    },
    adresse: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class CourseUpdate extends Vue {
  @Inject('courseService') private courseService: () => CourseService;
  @Inject('alertService') private alertService: () => AlertService;

  public course: ICourse = new Course();

  @Inject('commandeService') private commandeService: () => CommandeService;

  public commandes: ICommande[] = [];

  @Inject('coursierService') private coursierService: () => CoursierService;

  public coursiers: ICoursier[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.courseId) {
        vm.retrieveCourse(to.params.courseId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.course.id) {
      this.courseService()
        .update(this.course)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.course.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.courseService()
        .create(this.course)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.course.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveCourse(courseId): void {
    this.courseService()
      .find(courseId)
      .then(res => {
        this.course = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.commandeService()
      .retrieve()
      .then(res => {
        this.commandes = res.data;
      });
    this.coursierService()
      .retrieve()
      .then(res => {
        this.coursiers = res.data;
      });
  }
}
