package info4.gl.coopcycle.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A ProduitCommande.
 */
@Entity
@Table(name = "produit_commande")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ProduitCommande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_produit", nullable = false)
    private Long idProduit;

    @NotNull
    @Column(name = "id_commande", nullable = false)
    private Long idCommande;

    @Column(name = "quantite")
    private Integer quantite;

    @Column(name = "quantite_possible")
    private Boolean quantitePossible;

    @ManyToMany
    @NotNull
    @JoinTable(
        name = "rel_produit_commande__product",
        joinColumns = @JoinColumn(name = "produit_commande_id"),
        inverseJoinColumns = @JoinColumn(name = "product_id")
    )
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "menu", "produitcommandes" }, allowSetters = true)
    private Set<Produit> products = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "produitCommandes", "paiement", "course", "client", "cooperative" }, allowSetters = true)
    private Commande commande;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public ProduitCommande id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdProduit() {
        return this.idProduit;
    }

    public ProduitCommande idProduit(Long idProduit) {
        this.setIdProduit(idProduit);
        return this;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }

    public Long getIdCommande() {
        return this.idCommande;
    }

    public ProduitCommande idCommande(Long idCommande) {
        this.setIdCommande(idCommande);
        return this;
    }

    public void setIdCommande(Long idCommande) {
        this.idCommande = idCommande;
    }

    public Integer getQuantite() {
        return this.quantite;
    }

    public ProduitCommande quantite(Integer quantite) {
        this.setQuantite(quantite);
        return this;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public Boolean getQuantitePossible() {
        return this.quantitePossible;
    }

    public ProduitCommande quantitePossible(Boolean quantitePossible) {
        this.setQuantitePossible(quantitePossible);
        return this;
    }

    public void setQuantitePossible(Boolean quantitePossible) {
        this.quantitePossible = quantitePossible;
    }

    public Set<Produit> getProducts() {
        return this.products;
    }

    public void setProducts(Set<Produit> produits) {
        this.products = produits;
    }

    public ProduitCommande products(Set<Produit> produits) {
        this.setProducts(produits);
        return this;
    }

    public ProduitCommande addProduct(Produit produit) {
        this.products.add(produit);
        produit.getProduitcommandes().add(this);
        return this;
    }

    public ProduitCommande removeProduct(Produit produit) {
        this.products.remove(produit);
        produit.getProduitcommandes().remove(this);
        return this;
    }

    public Commande getCommande() {
        return this.commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    public ProduitCommande commande(Commande commande) {
        this.setCommande(commande);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProduitCommande)) {
            return false;
        }
        return id != null && id.equals(((ProduitCommande) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProduitCommande{" +
            "id=" + getId() +
            ", idProduit=" + getIdProduit() +
            ", idCommande=" + getIdCommande() +
            ", quantite=" + getQuantite() +
            ", quantitePossible='" + getQuantitePossible() + "'" +
            "}";
    }
}
