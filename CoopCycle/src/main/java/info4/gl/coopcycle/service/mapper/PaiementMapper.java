package info4.gl.coopcycle.service.mapper;

import info4.gl.coopcycle.domain.Commande;
import info4.gl.coopcycle.domain.Paiement;
import info4.gl.coopcycle.service.dto.CommandeDTO;
import info4.gl.coopcycle.service.dto.PaiementDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Paiement} and its DTO {@link PaiementDTO}.
 */
@Mapper(componentModel = "spring")
public interface PaiementMapper extends EntityMapper<PaiementDTO, Paiement> {
    @Mapping(target = "commande", source = "commande", qualifiedByName = "commandeId")
    PaiementDTO toDto(Paiement s);

    @Named("commandeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CommandeDTO toDtoCommandeId(Commande commande);
}
