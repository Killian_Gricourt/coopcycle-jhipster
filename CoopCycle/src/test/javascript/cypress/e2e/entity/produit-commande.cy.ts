import {
  entityTableSelector,
  entityDetailsButtonSelector,
  entityDetailsBackButtonSelector,
  entityCreateButtonSelector,
  entityCreateSaveButtonSelector,
  entityCreateCancelButtonSelector,
  entityEditButtonSelector,
  entityDeleteButtonSelector,
  entityConfirmDeleteButtonSelector,
} from '../../support/entity';

describe('ProduitCommande e2e test', () => {
  const produitCommandePageUrl = '/produit-commande';
  const produitCommandePageUrlPattern = new RegExp('/produit-commande(\\?.*)?$');
  const username = Cypress.env('E2E_USERNAME') ?? 'user';
  const password = Cypress.env('E2E_PASSWORD') ?? 'user';
  const produitCommandeSample = { idProduit: 28493, idCommande: 78623 };

  let produitCommande;
  let produit;

  beforeEach(() => {
    cy.login(username, password);
  });

  beforeEach(() => {
    // create an instance at the required relationship entity:
    cy.authenticatedRequest({
      method: 'POST',
      url: '/api/produits',
      body: { idProduit: 1556, idMenu: 58054, nom: 'solid Palladium', prix: 1158, nbStock: 18342 },
    }).then(({ body }) => {
      produit = body;
    });
  });

  beforeEach(() => {
    cy.intercept('GET', '/api/produit-commandes+(?*|)').as('entitiesRequest');
    cy.intercept('POST', '/api/produit-commandes').as('postEntityRequest');
    cy.intercept('DELETE', '/api/produit-commandes/*').as('deleteEntityRequest');
  });

  beforeEach(() => {
    // Simulate relationships api for better performance and reproducibility.
    cy.intercept('GET', '/api/produits', {
      statusCode: 200,
      body: [produit],
    });

    cy.intercept('GET', '/api/commandes', {
      statusCode: 200,
      body: [],
    });
  });

  afterEach(() => {
    if (produitCommande) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/produit-commandes/${produitCommande.id}`,
      }).then(() => {
        produitCommande = undefined;
      });
    }
  });

  afterEach(() => {
    if (produit) {
      cy.authenticatedRequest({
        method: 'DELETE',
        url: `/api/produits/${produit.id}`,
      }).then(() => {
        produit = undefined;
      });
    }
  });

  it('ProduitCommandes menu should load ProduitCommandes page', () => {
    cy.visit('/');
    cy.clickOnEntityMenuItem('produit-commande');
    cy.wait('@entitiesRequest').then(({ response }) => {
      if (response.body.length === 0) {
        cy.get(entityTableSelector).should('not.exist');
      } else {
        cy.get(entityTableSelector).should('exist');
      }
    });
    cy.getEntityHeading('ProduitCommande').should('exist');
    cy.url().should('match', produitCommandePageUrlPattern);
  });

  describe('ProduitCommande page', () => {
    describe('create button click', () => {
      beforeEach(() => {
        cy.visit(produitCommandePageUrl);
        cy.wait('@entitiesRequest');
      });

      it('should load create ProduitCommande page', () => {
        cy.get(entityCreateButtonSelector).click();
        cy.url().should('match', new RegExp('/produit-commande/new$'));
        cy.getEntityCreateUpdateHeading('ProduitCommande');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', produitCommandePageUrlPattern);
      });
    });

    describe('with existing value', () => {
      beforeEach(() => {
        cy.authenticatedRequest({
          method: 'POST',
          url: '/api/produit-commandes',
          body: {
            ...produitCommandeSample,
            product: produit,
          },
        }).then(({ body }) => {
          produitCommande = body;

          cy.intercept(
            {
              method: 'GET',
              url: '/api/produit-commandes+(?*|)',
              times: 1,
            },
            {
              statusCode: 200,
              body: [produitCommande],
            }
          ).as('entitiesRequestInternal');
        });

        cy.visit(produitCommandePageUrl);

        cy.wait('@entitiesRequestInternal');
      });

      it('detail button click should load details ProduitCommande page', () => {
        cy.get(entityDetailsButtonSelector).first().click();
        cy.getEntityDetailsHeading('produitCommande');
        cy.get(entityDetailsBackButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', produitCommandePageUrlPattern);
      });

      it('edit button click should load edit ProduitCommande page and go back', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('ProduitCommande');
        cy.get(entityCreateSaveButtonSelector).should('exist');
        cy.get(entityCreateCancelButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', produitCommandePageUrlPattern);
      });

      it('edit button click should load edit ProduitCommande page and save', () => {
        cy.get(entityEditButtonSelector).first().click();
        cy.getEntityCreateUpdateHeading('ProduitCommande');
        cy.get(entityCreateSaveButtonSelector).click();
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', produitCommandePageUrlPattern);
      });

      it('last delete button click should delete instance of ProduitCommande', () => {
        cy.get(entityDeleteButtonSelector).last().click();
        cy.getEntityDeleteDialogHeading('produitCommande').should('exist');
        cy.get(entityConfirmDeleteButtonSelector).click();
        cy.wait('@deleteEntityRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(204);
        });
        cy.wait('@entitiesRequest').then(({ response }) => {
          expect(response.statusCode).to.equal(200);
        });
        cy.url().should('match', produitCommandePageUrlPattern);

        produitCommande = undefined;
      });
    });
  });

  describe('new ProduitCommande page', () => {
    beforeEach(() => {
      cy.visit(`${produitCommandePageUrl}`);
      cy.get(entityCreateButtonSelector).click();
      cy.getEntityCreateUpdateHeading('ProduitCommande');
    });

    it('should create an instance of ProduitCommande', () => {
      cy.get(`[data-cy="idProduit"]`).type('35282').should('have.value', '35282');

      cy.get(`[data-cy="idCommande"]`).type('89428').should('have.value', '89428');

      cy.get(`[data-cy="quantite"]`).type('36481').should('have.value', '36481');

      cy.get(`[data-cy="quantitePossible"]`).should('not.be.checked');
      cy.get(`[data-cy="quantitePossible"]`).click().should('be.checked');

      cy.get(`[data-cy="product"]`).select([0]);

      cy.get(entityCreateSaveButtonSelector).click();

      cy.wait('@postEntityRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(201);
        produitCommande = response.body;
      });
      cy.wait('@entitiesRequest').then(({ response }) => {
        expect(response.statusCode).to.equal(200);
      });
      cy.url().should('match', produitCommandePageUrlPattern);
    });
  });
});
