package info4.gl.coopcycle.service.mapper;

import info4.gl.coopcycle.domain.Coursier;
import info4.gl.coopcycle.service.dto.CoursierDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Coursier} and its DTO {@link CoursierDTO}.
 */
@Mapper(componentModel = "spring")
public interface CoursierMapper extends EntityMapper<CoursierDTO, Coursier> {}
