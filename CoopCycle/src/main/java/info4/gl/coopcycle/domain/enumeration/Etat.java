package info4.gl.coopcycle.domain.enumeration;

/**
 * The Etat enumeration.
 */
public enum Etat {
    PREPARATION,
    EMPORTE,
    ENCOURSDELIVRAISON,
    LIVRE,
    PAYE,
}
