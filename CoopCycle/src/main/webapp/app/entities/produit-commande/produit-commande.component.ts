import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IProduitCommande } from '@/shared/model/produit-commande.model';

import ProduitCommandeService from './produit-commande.service';
import AlertService from '@/shared/alert/alert.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class ProduitCommande extends Vue {
  @Inject('produitCommandeService') private produitCommandeService: () => ProduitCommandeService;
  @Inject('alertService') private alertService: () => AlertService;

  private removeId: number = null;

  public produitCommandes: IProduitCommande[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllProduitCommandes();
  }

  public clear(): void {
    this.retrieveAllProduitCommandes();
  }

  public retrieveAllProduitCommandes(): void {
    this.isFetching = true;
    this.produitCommandeService()
      .retrieve()
      .then(
        res => {
          this.produitCommandes = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
          this.alertService().showHttpError(this, err.response);
        }
      );
  }

  public handleSyncList(): void {
    this.clear();
  }

  public prepareRemove(instance: IProduitCommande): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeProduitCommande(): void {
    this.produitCommandeService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('coopCycleApp.produitCommande.deleted', { param: this.removeId });
        this.$bvToast.toast(message.toString(), {
          toaster: 'b-toaster-top-center',
          title: 'Info',
          variant: 'danger',
          solid: true,
          autoHideDelay: 5000,
        });
        this.removeId = null;
        this.retrieveAllProduitCommandes();
        this.closeDialog();
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
