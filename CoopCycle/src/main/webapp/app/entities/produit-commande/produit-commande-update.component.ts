import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';

import ProduitService from '@/entities/produit/produit.service';
import { IProduit } from '@/shared/model/produit.model';

import CommandeService from '@/entities/commande/commande.service';
import { ICommande } from '@/shared/model/commande.model';

import { IProduitCommande, ProduitCommande } from '@/shared/model/produit-commande.model';
import ProduitCommandeService from './produit-commande.service';

const validations: any = {
  produitCommande: {
    idProduit: {
      required,
      numeric,
    },
    idCommande: {
      required,
      numeric,
    },
    quantite: {},
    quantitePossible: {},
    products: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class ProduitCommandeUpdate extends Vue {
  @Inject('produitCommandeService') private produitCommandeService: () => ProduitCommandeService;
  @Inject('alertService') private alertService: () => AlertService;

  public produitCommande: IProduitCommande = new ProduitCommande();

  @Inject('produitService') private produitService: () => ProduitService;

  public produits: IProduit[] = [];

  @Inject('commandeService') private commandeService: () => CommandeService;

  public commandes: ICommande[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.produitCommandeId) {
        vm.retrieveProduitCommande(to.params.produitCommandeId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.produitCommande.products = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.produitCommande.id) {
      this.produitCommandeService()
        .update(this.produitCommande)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.produitCommande.updated', { param: param.id });
          return (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Info',
            variant: 'info',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    } else {
      this.produitCommandeService()
        .create(this.produitCommande)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coopCycleApp.produitCommande.created', { param: param.id });
          (this.$root as any).$bvToast.toast(message.toString(), {
            toaster: 'b-toaster-top-center',
            title: 'Success',
            variant: 'success',
            solid: true,
            autoHideDelay: 5000,
          });
        })
        .catch(error => {
          this.isSaving = false;
          this.alertService().showHttpError(this, error.response);
        });
    }
  }

  public retrieveProduitCommande(produitCommandeId): void {
    this.produitCommandeService()
      .find(produitCommandeId)
      .then(res => {
        this.produitCommande = res;
      })
      .catch(error => {
        this.alertService().showHttpError(this, error.response);
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.produitService()
      .retrieve()
      .then(res => {
        this.produits = res.data;
      });
    this.commandeService()
      .retrieve()
      .then(res => {
        this.commandes = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      return selectedVals.find(value => option.id === value.id) ?? option;
    }
    return option;
  }
}
