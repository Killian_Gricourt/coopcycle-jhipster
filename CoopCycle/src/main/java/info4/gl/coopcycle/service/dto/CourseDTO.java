package info4.gl.coopcycle.service.dto;

import java.io.Serializable;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link info4.gl.coopcycle.domain.Course} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class CourseDTO implements Serializable {

    @NotNull
    private Long id;

    @NotNull
    private Long idCoursier;

    @NotNull
    private Long idCommande;

    @NotNull
    private String adresse;

    private CommandeDTO commande;

    private CoursierDTO coursier;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCoursier() {
        return idCoursier;
    }

    public void setIdCoursier(Long idCoursier) {
        this.idCoursier = idCoursier;
    }

    public Long getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(Long idCommande) {
        this.idCommande = idCommande;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public CommandeDTO getCommande() {
        return commande;
    }

    public void setCommande(CommandeDTO commande) {
        this.commande = commande;
    }

    public CoursierDTO getCoursier() {
        return coursier;
    }

    public void setCoursier(CoursierDTO coursier) {
        this.coursier = coursier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CourseDTO)) {
            return false;
        }

        CourseDTO courseDTO = (CourseDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, courseDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "CourseDTO{" +
            "id=" + getId() +
            ", idCoursier=" + getIdCoursier() +
            ", idCommande=" + getIdCommande() +
            ", adresse='" + getAdresse() + "'" +
            ", commande=" + getCommande() +
            ", coursier=" + getCoursier() +
            "}";
    }
}
