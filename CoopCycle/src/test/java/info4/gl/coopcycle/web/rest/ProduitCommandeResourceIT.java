package info4.gl.coopcycle.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import info4.gl.coopcycle.IntegrationTest;
import info4.gl.coopcycle.domain.Produit;
import info4.gl.coopcycle.domain.ProduitCommande;
import info4.gl.coopcycle.repository.ProduitCommandeRepository;
import info4.gl.coopcycle.service.ProduitCommandeService;
import info4.gl.coopcycle.service.dto.ProduitCommandeDTO;
import info4.gl.coopcycle.service.mapper.ProduitCommandeMapper;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ProduitCommandeResource} REST controller.
 */
@IntegrationTest
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
class ProduitCommandeResourceIT {

    private static final Long DEFAULT_ID_PRODUIT = 1L;
    private static final Long UPDATED_ID_PRODUIT = 2L;

    private static final Long DEFAULT_ID_COMMANDE = 1L;
    private static final Long UPDATED_ID_COMMANDE = 2L;

    private static final Integer DEFAULT_QUANTITE = 1;
    private static final Integer UPDATED_QUANTITE = 2;

    private static final Boolean DEFAULT_QUANTITE_POSSIBLE = false;
    private static final Boolean UPDATED_QUANTITE_POSSIBLE = true;

    private static final String ENTITY_API_URL = "/api/produit-commandes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ProduitCommandeRepository produitCommandeRepository;

    @Mock
    private ProduitCommandeRepository produitCommandeRepositoryMock;

    @Autowired
    private ProduitCommandeMapper produitCommandeMapper;

    @Mock
    private ProduitCommandeService produitCommandeServiceMock;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restProduitCommandeMockMvc;

    private ProduitCommande produitCommande;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProduitCommande createEntity(EntityManager em) {
        ProduitCommande produitCommande = new ProduitCommande()
            .idProduit(DEFAULT_ID_PRODUIT)
            .idCommande(DEFAULT_ID_COMMANDE)
            .quantite(DEFAULT_QUANTITE)
            .quantitePossible(DEFAULT_QUANTITE_POSSIBLE);
        // Add required entity
        Produit produit;
        if (TestUtil.findAll(em, Produit.class).isEmpty()) {
            produit = ProduitResourceIT.createEntity(em);
            em.persist(produit);
            em.flush();
        } else {
            produit = TestUtil.findAll(em, Produit.class).get(0);
        }
        produitCommande.getProducts().add(produit);
        return produitCommande;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ProduitCommande createUpdatedEntity(EntityManager em) {
        ProduitCommande produitCommande = new ProduitCommande()
            .idProduit(UPDATED_ID_PRODUIT)
            .idCommande(UPDATED_ID_COMMANDE)
            .quantite(UPDATED_QUANTITE)
            .quantitePossible(UPDATED_QUANTITE_POSSIBLE);
        // Add required entity
        Produit produit;
        if (TestUtil.findAll(em, Produit.class).isEmpty()) {
            produit = ProduitResourceIT.createUpdatedEntity(em);
            em.persist(produit);
            em.flush();
        } else {
            produit = TestUtil.findAll(em, Produit.class).get(0);
        }
        produitCommande.getProducts().add(produit);
        return produitCommande;
    }

    @BeforeEach
    public void initTest() {
        produitCommande = createEntity(em);
    }

    @Test
    @Transactional
    void createProduitCommande() throws Exception {
        int databaseSizeBeforeCreate = produitCommandeRepository.findAll().size();
        // Create the ProduitCommande
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);
        restProduitCommandeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isCreated());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeCreate + 1);
        ProduitCommande testProduitCommande = produitCommandeList.get(produitCommandeList.size() - 1);
        assertThat(testProduitCommande.getIdProduit()).isEqualTo(DEFAULT_ID_PRODUIT);
        assertThat(testProduitCommande.getIdCommande()).isEqualTo(DEFAULT_ID_COMMANDE);
        assertThat(testProduitCommande.getQuantite()).isEqualTo(DEFAULT_QUANTITE);
        assertThat(testProduitCommande.getQuantitePossible()).isEqualTo(DEFAULT_QUANTITE_POSSIBLE);
    }

    @Test
    @Transactional
    void createProduitCommandeWithExistingId() throws Exception {
        // Create the ProduitCommande with an existing ID
        produitCommande.setId(1L);
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);

        int databaseSizeBeforeCreate = produitCommandeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restProduitCommandeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkIdProduitIsRequired() throws Exception {
        int databaseSizeBeforeTest = produitCommandeRepository.findAll().size();
        // set the field null
        produitCommande.setIdProduit(null);

        // Create the ProduitCommande, which fails.
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);

        restProduitCommandeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isBadRequest());

        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIdCommandeIsRequired() throws Exception {
        int databaseSizeBeforeTest = produitCommandeRepository.findAll().size();
        // set the field null
        produitCommande.setIdCommande(null);

        // Create the ProduitCommande, which fails.
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);

        restProduitCommandeMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isBadRequest());

        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllProduitCommandes() throws Exception {
        // Initialize the database
        produitCommandeRepository.saveAndFlush(produitCommande);

        // Get all the produitCommandeList
        restProduitCommandeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(produitCommande.getId().intValue())))
            .andExpect(jsonPath("$.[*].idProduit").value(hasItem(DEFAULT_ID_PRODUIT.intValue())))
            .andExpect(jsonPath("$.[*].idCommande").value(hasItem(DEFAULT_ID_COMMANDE.intValue())))
            .andExpect(jsonPath("$.[*].quantite").value(hasItem(DEFAULT_QUANTITE)))
            .andExpect(jsonPath("$.[*].quantitePossible").value(hasItem(DEFAULT_QUANTITE_POSSIBLE.booleanValue())));
    }

    @SuppressWarnings({ "unchecked" })
    void getAllProduitCommandesWithEagerRelationshipsIsEnabled() throws Exception {
        when(produitCommandeServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restProduitCommandeMockMvc.perform(get(ENTITY_API_URL + "?eagerload=true")).andExpect(status().isOk());

        verify(produitCommandeServiceMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({ "unchecked" })
    void getAllProduitCommandesWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(produitCommandeServiceMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restProduitCommandeMockMvc.perform(get(ENTITY_API_URL + "?eagerload=false")).andExpect(status().isOk());
        verify(produitCommandeRepositoryMock, times(1)).findAll(any(Pageable.class));
    }

    @Test
    @Transactional
    void getProduitCommande() throws Exception {
        // Initialize the database
        produitCommandeRepository.saveAndFlush(produitCommande);

        // Get the produitCommande
        restProduitCommandeMockMvc
            .perform(get(ENTITY_API_URL_ID, produitCommande.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(produitCommande.getId().intValue()))
            .andExpect(jsonPath("$.idProduit").value(DEFAULT_ID_PRODUIT.intValue()))
            .andExpect(jsonPath("$.idCommande").value(DEFAULT_ID_COMMANDE.intValue()))
            .andExpect(jsonPath("$.quantite").value(DEFAULT_QUANTITE))
            .andExpect(jsonPath("$.quantitePossible").value(DEFAULT_QUANTITE_POSSIBLE.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingProduitCommande() throws Exception {
        // Get the produitCommande
        restProduitCommandeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putExistingProduitCommande() throws Exception {
        // Initialize the database
        produitCommandeRepository.saveAndFlush(produitCommande);

        int databaseSizeBeforeUpdate = produitCommandeRepository.findAll().size();

        // Update the produitCommande
        ProduitCommande updatedProduitCommande = produitCommandeRepository.findById(produitCommande.getId()).get();
        // Disconnect from session so that the updates on updatedProduitCommande are not directly saved in db
        em.detach(updatedProduitCommande);
        updatedProduitCommande
            .idProduit(UPDATED_ID_PRODUIT)
            .idCommande(UPDATED_ID_COMMANDE)
            .quantite(UPDATED_QUANTITE)
            .quantitePossible(UPDATED_QUANTITE_POSSIBLE);
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(updatedProduitCommande);

        restProduitCommandeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, produitCommandeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isOk());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeUpdate);
        ProduitCommande testProduitCommande = produitCommandeList.get(produitCommandeList.size() - 1);
        assertThat(testProduitCommande.getIdProduit()).isEqualTo(UPDATED_ID_PRODUIT);
        assertThat(testProduitCommande.getIdCommande()).isEqualTo(UPDATED_ID_COMMANDE);
        assertThat(testProduitCommande.getQuantite()).isEqualTo(UPDATED_QUANTITE);
        assertThat(testProduitCommande.getQuantitePossible()).isEqualTo(UPDATED_QUANTITE_POSSIBLE);
    }

    @Test
    @Transactional
    void putNonExistingProduitCommande() throws Exception {
        int databaseSizeBeforeUpdate = produitCommandeRepository.findAll().size();
        produitCommande.setId(count.incrementAndGet());

        // Create the ProduitCommande
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProduitCommandeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, produitCommandeDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchProduitCommande() throws Exception {
        int databaseSizeBeforeUpdate = produitCommandeRepository.findAll().size();
        produitCommande.setId(count.incrementAndGet());

        // Create the ProduitCommande
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProduitCommandeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamProduitCommande() throws Exception {
        int databaseSizeBeforeUpdate = produitCommandeRepository.findAll().size();
        produitCommande.setId(count.incrementAndGet());

        // Create the ProduitCommande
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProduitCommandeMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateProduitCommandeWithPatch() throws Exception {
        // Initialize the database
        produitCommandeRepository.saveAndFlush(produitCommande);

        int databaseSizeBeforeUpdate = produitCommandeRepository.findAll().size();

        // Update the produitCommande using partial update
        ProduitCommande partialUpdatedProduitCommande = new ProduitCommande();
        partialUpdatedProduitCommande.setId(produitCommande.getId());

        partialUpdatedProduitCommande.idProduit(UPDATED_ID_PRODUIT).idCommande(UPDATED_ID_COMMANDE);

        restProduitCommandeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProduitCommande.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProduitCommande))
            )
            .andExpect(status().isOk());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeUpdate);
        ProduitCommande testProduitCommande = produitCommandeList.get(produitCommandeList.size() - 1);
        assertThat(testProduitCommande.getIdProduit()).isEqualTo(UPDATED_ID_PRODUIT);
        assertThat(testProduitCommande.getIdCommande()).isEqualTo(UPDATED_ID_COMMANDE);
        assertThat(testProduitCommande.getQuantite()).isEqualTo(DEFAULT_QUANTITE);
        assertThat(testProduitCommande.getQuantitePossible()).isEqualTo(DEFAULT_QUANTITE_POSSIBLE);
    }

    @Test
    @Transactional
    void fullUpdateProduitCommandeWithPatch() throws Exception {
        // Initialize the database
        produitCommandeRepository.saveAndFlush(produitCommande);

        int databaseSizeBeforeUpdate = produitCommandeRepository.findAll().size();

        // Update the produitCommande using partial update
        ProduitCommande partialUpdatedProduitCommande = new ProduitCommande();
        partialUpdatedProduitCommande.setId(produitCommande.getId());

        partialUpdatedProduitCommande
            .idProduit(UPDATED_ID_PRODUIT)
            .idCommande(UPDATED_ID_COMMANDE)
            .quantite(UPDATED_QUANTITE)
            .quantitePossible(UPDATED_QUANTITE_POSSIBLE);

        restProduitCommandeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedProduitCommande.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedProduitCommande))
            )
            .andExpect(status().isOk());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeUpdate);
        ProduitCommande testProduitCommande = produitCommandeList.get(produitCommandeList.size() - 1);
        assertThat(testProduitCommande.getIdProduit()).isEqualTo(UPDATED_ID_PRODUIT);
        assertThat(testProduitCommande.getIdCommande()).isEqualTo(UPDATED_ID_COMMANDE);
        assertThat(testProduitCommande.getQuantite()).isEqualTo(UPDATED_QUANTITE);
        assertThat(testProduitCommande.getQuantitePossible()).isEqualTo(UPDATED_QUANTITE_POSSIBLE);
    }

    @Test
    @Transactional
    void patchNonExistingProduitCommande() throws Exception {
        int databaseSizeBeforeUpdate = produitCommandeRepository.findAll().size();
        produitCommande.setId(count.incrementAndGet());

        // Create the ProduitCommande
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restProduitCommandeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, produitCommandeDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchProduitCommande() throws Exception {
        int databaseSizeBeforeUpdate = produitCommandeRepository.findAll().size();
        produitCommande.setId(count.incrementAndGet());

        // Create the ProduitCommande
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProduitCommandeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamProduitCommande() throws Exception {
        int databaseSizeBeforeUpdate = produitCommandeRepository.findAll().size();
        produitCommande.setId(count.incrementAndGet());

        // Create the ProduitCommande
        ProduitCommandeDTO produitCommandeDTO = produitCommandeMapper.toDto(produitCommande);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restProduitCommandeMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(produitCommandeDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ProduitCommande in the database
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteProduitCommande() throws Exception {
        // Initialize the database
        produitCommandeRepository.saveAndFlush(produitCommande);

        int databaseSizeBeforeDelete = produitCommandeRepository.findAll().size();

        // Delete the produitCommande
        restProduitCommandeMockMvc
            .perform(delete(ENTITY_API_URL_ID, produitCommande.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ProduitCommande> produitCommandeList = produitCommandeRepository.findAll();
        assertThat(produitCommandeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
