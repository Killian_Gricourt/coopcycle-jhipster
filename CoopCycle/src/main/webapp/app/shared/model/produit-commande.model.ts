import { IProduit } from '@/shared/model/produit.model';
import { ICommande } from '@/shared/model/commande.model';

export interface IProduitCommande {
  id?: number;
  idProduit?: number;
  idCommande?: number;
  quantite?: number | null;
  quantitePossible?: boolean | null;
  products?: IProduit[];
  commande?: ICommande | null;
}

export class ProduitCommande implements IProduitCommande {
  constructor(
    public id?: number,
    public idProduit?: number,
    public idCommande?: number,
    public quantite?: number | null,
    public quantitePossible?: boolean | null,
    public products?: IProduit[],
    public commande?: ICommande | null
  ) {
    this.quantitePossible = this.quantitePossible ?? false;
  }
}
