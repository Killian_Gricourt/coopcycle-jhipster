import { Component, Provide, Vue } from 'vue-property-decorator';

import UserService from '@/entities/user/user.service';
import CooperativeService from './cooperative/cooperative.service';
import ClientService from './client/client.service';
import CoursierService from './coursier/coursier.service';
import CourseService from './course/course.service';
import MenuService from './menu/menu.service';
import CommandeService from './commande/commande.service';
import ProduitService from './produit/produit.service';
import ProduitCommandeService from './produit-commande/produit-commande.service';
import PaiementService from './paiement/paiement.service';
// jhipster-needle-add-entity-service-to-entities-component-import - JHipster will import entities services here

@Component
export default class Entities extends Vue {
  @Provide('userService') private userService = () => new UserService();
  @Provide('cooperativeService') private cooperativeService = () => new CooperativeService();
  @Provide('clientService') private clientService = () => new ClientService();
  @Provide('coursierService') private coursierService = () => new CoursierService();
  @Provide('courseService') private courseService = () => new CourseService();
  @Provide('menuService') private menuService = () => new MenuService();
  @Provide('commandeService') private commandeService = () => new CommandeService();
  @Provide('produitService') private produitService = () => new ProduitService();
  @Provide('produitCommandeService') private produitCommandeService = () => new ProduitCommandeService();
  @Provide('paiementService') private paiementService = () => new PaiementService();
  // jhipster-needle-add-entity-service-to-entities-component - JHipster will import entities services here
}
