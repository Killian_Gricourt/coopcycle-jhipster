import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore
const Entities = () => import('@/entities/entities.vue');

// prettier-ignore
const Cooperative = () => import('@/entities/cooperative/cooperative.vue');
// prettier-ignore
const CooperativeUpdate = () => import('@/entities/cooperative/cooperative-update.vue');
// prettier-ignore
const CooperativeDetails = () => import('@/entities/cooperative/cooperative-details.vue');
// prettier-ignore
const Client = () => import('@/entities/client/client.vue');
// prettier-ignore
const ClientUpdate = () => import('@/entities/client/client-update.vue');
// prettier-ignore
const ClientDetails = () => import('@/entities/client/client-details.vue');
// prettier-ignore
const Coursier = () => import('@/entities/coursier/coursier.vue');
// prettier-ignore
const CoursierUpdate = () => import('@/entities/coursier/coursier-update.vue');
// prettier-ignore
const CoursierDetails = () => import('@/entities/coursier/coursier-details.vue');
// prettier-ignore
const Course = () => import('@/entities/course/course.vue');
// prettier-ignore
const CourseUpdate = () => import('@/entities/course/course-update.vue');
// prettier-ignore
const CourseDetails = () => import('@/entities/course/course-details.vue');
// prettier-ignore
const Menu = () => import('@/entities/menu/menu.vue');
// prettier-ignore
const MenuUpdate = () => import('@/entities/menu/menu-update.vue');
// prettier-ignore
const MenuDetails = () => import('@/entities/menu/menu-details.vue');
// prettier-ignore
const Commande = () => import('@/entities/commande/commande.vue');
// prettier-ignore
const CommandeUpdate = () => import('@/entities/commande/commande-update.vue');
// prettier-ignore
const CommandeDetails = () => import('@/entities/commande/commande-details.vue');
// prettier-ignore
const Produit = () => import('@/entities/produit/produit.vue');
// prettier-ignore
const ProduitUpdate = () => import('@/entities/produit/produit-update.vue');
// prettier-ignore
const ProduitDetails = () => import('@/entities/produit/produit-details.vue');
// prettier-ignore
const ProduitCommande = () => import('@/entities/produit-commande/produit-commande.vue');
// prettier-ignore
const ProduitCommandeUpdate = () => import('@/entities/produit-commande/produit-commande-update.vue');
// prettier-ignore
const ProduitCommandeDetails = () => import('@/entities/produit-commande/produit-commande-details.vue');
// prettier-ignore
const Paiement = () => import('@/entities/paiement/paiement.vue');
// prettier-ignore
const PaiementUpdate = () => import('@/entities/paiement/paiement-update.vue');
// prettier-ignore
const PaiementDetails = () => import('@/entities/paiement/paiement-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

export default {
  path: '/',
  component: Entities,
  children: [
    {
      path: 'cooperative',
      name: 'Cooperative',
      component: Cooperative,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/new',
      name: 'CooperativeCreate',
      component: CooperativeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/:cooperativeId/edit',
      name: 'CooperativeEdit',
      component: CooperativeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'cooperative/:cooperativeId/view',
      name: 'CooperativeView',
      component: CooperativeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client',
      name: 'Client',
      component: Client,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client/new',
      name: 'ClientCreate',
      component: ClientUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client/:clientId/edit',
      name: 'ClientEdit',
      component: ClientUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'client/:clientId/view',
      name: 'ClientView',
      component: ClientDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coursier',
      name: 'Coursier',
      component: Coursier,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coursier/new',
      name: 'CoursierCreate',
      component: CoursierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coursier/:coursierId/edit',
      name: 'CoursierEdit',
      component: CoursierUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'coursier/:coursierId/view',
      name: 'CoursierView',
      component: CoursierDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course',
      name: 'Course',
      component: Course,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course/new',
      name: 'CourseCreate',
      component: CourseUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course/:courseId/edit',
      name: 'CourseEdit',
      component: CourseUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'course/:courseId/view',
      name: 'CourseView',
      component: CourseDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'menu',
      name: 'Menu',
      component: Menu,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'menu/new',
      name: 'MenuCreate',
      component: MenuUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'menu/:menuId/edit',
      name: 'MenuEdit',
      component: MenuUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'menu/:menuId/view',
      name: 'MenuView',
      component: MenuDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'commande',
      name: 'Commande',
      component: Commande,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'commande/new',
      name: 'CommandeCreate',
      component: CommandeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'commande/:commandeId/edit',
      name: 'CommandeEdit',
      component: CommandeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'commande/:commandeId/view',
      name: 'CommandeView',
      component: CommandeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit',
      name: 'Produit',
      component: Produit,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit/new',
      name: 'ProduitCreate',
      component: ProduitUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit/:produitId/edit',
      name: 'ProduitEdit',
      component: ProduitUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit/:produitId/view',
      name: 'ProduitView',
      component: ProduitDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit-commande',
      name: 'ProduitCommande',
      component: ProduitCommande,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit-commande/new',
      name: 'ProduitCommandeCreate',
      component: ProduitCommandeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit-commande/:produitCommandeId/edit',
      name: 'ProduitCommandeEdit',
      component: ProduitCommandeUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'produit-commande/:produitCommandeId/view',
      name: 'ProduitCommandeView',
      component: ProduitCommandeDetails,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement',
      name: 'Paiement',
      component: Paiement,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement/new',
      name: 'PaiementCreate',
      component: PaiementUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement/:paiementId/edit',
      name: 'PaiementEdit',
      component: PaiementUpdate,
      meta: { authorities: [Authority.USER] },
    },
    {
      path: 'paiement/:paiementId/view',
      name: 'PaiementView',
      component: PaiementDetails,
      meta: { authorities: [Authority.USER] },
    },
    // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
  ],
};
