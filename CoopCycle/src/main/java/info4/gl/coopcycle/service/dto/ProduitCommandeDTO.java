package info4.gl.coopcycle.service.dto;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link info4.gl.coopcycle.domain.ProduitCommande} entity.
 */
@SuppressWarnings("common-java:DuplicatedBlocks")
public class ProduitCommandeDTO implements Serializable {

    private Long id;

    @NotNull
    private Long idProduit;

    @NotNull
    private Long idCommande;

    private Integer quantite;

    private Boolean quantitePossible;

    private Set<ProduitDTO> products = new HashSet<>();

    private CommandeDTO commande;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdProduit() {
        return idProduit;
    }

    public void setIdProduit(Long idProduit) {
        this.idProduit = idProduit;
    }

    public Long getIdCommande() {
        return idCommande;
    }

    public void setIdCommande(Long idCommande) {
        this.idCommande = idCommande;
    }

    public Integer getQuantite() {
        return quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public Boolean getQuantitePossible() {
        return quantitePossible;
    }

    public void setQuantitePossible(Boolean quantitePossible) {
        this.quantitePossible = quantitePossible;
    }

    public Set<ProduitDTO> getProducts() {
        return products;
    }

    public void setProducts(Set<ProduitDTO> products) {
        this.products = products;
    }

    public CommandeDTO getCommande() {
        return commande;
    }

    public void setCommande(CommandeDTO commande) {
        this.commande = commande;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ProduitCommandeDTO)) {
            return false;
        }

        ProduitCommandeDTO produitCommandeDTO = (ProduitCommandeDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, produitCommandeDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ProduitCommandeDTO{" +
            "id=" + getId() +
            ", idProduit=" + getIdProduit() +
            ", idCommande=" + getIdCommande() +
            ", quantite=" + getQuantite() +
            ", quantitePossible='" + getQuantitePossible() + "'" +
            ", products=" + getProducts() +
            ", commande=" + getCommande() +
            "}";
    }
}
