import { IMenu } from '@/shared/model/menu.model';
import { IProduitCommande } from '@/shared/model/produit-commande.model';

export interface IProduit {
  id?: number;
  idProduit?: number;
  idMenu?: number;
  nom?: string | null;
  prix?: number;
  nbStock?: number | null;
  menu?: IMenu | null;
  produitcommandes?: IProduitCommande[] | null;
}

export class Produit implements IProduit {
  constructor(
    public id?: number,
    public idProduit?: number,
    public idMenu?: number,
    public nom?: string | null,
    public prix?: number,
    public nbStock?: number | null,
    public menu?: IMenu | null,
    public produitcommandes?: IProduitCommande[] | null
  ) {}
}
