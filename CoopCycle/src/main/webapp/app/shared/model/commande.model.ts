import { IProduitCommande } from '@/shared/model/produit-commande.model';
import { IPaiement } from '@/shared/model/paiement.model';
import { ICourse } from '@/shared/model/course.model';
import { IClient } from '@/shared/model/client.model';
import { ICooperative } from '@/shared/model/cooperative.model';

import { Etat } from '@/shared/model/enumerations/etat.model';
export interface ICommande {
  id?: number;
  idCommande?: number;
  idCooperative?: number;
  idClient?: number;
  idCourse?: number;
  prix?: number;
  date?: Date | null;
  etat?: Etat;
  produitCommandes?: IProduitCommande[] | null;
  paiement?: IPaiement | null;
  course?: ICourse | null;
  client?: IClient | null;
  cooperative?: ICooperative | null;
}

export class Commande implements ICommande {
  constructor(
    public id?: number,
    public idCommande?: number,
    public idCooperative?: number,
    public idClient?: number,
    public idCourse?: number,
    public prix?: number,
    public date?: Date | null,
    public etat?: Etat,
    public produitCommandes?: IProduitCommande[] | null,
    public paiement?: IPaiement | null,
    public course?: ICourse | null,
    public client?: IClient | null,
    public cooperative?: ICooperative | null
  ) {}
}
