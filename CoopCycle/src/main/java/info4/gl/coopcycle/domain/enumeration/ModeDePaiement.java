package info4.gl.coopcycle.domain.enumeration;

/**
 * The ModeDePaiement enumeration.
 */
public enum ModeDePaiement {
    CB,
    BITCOIN,
    PAYPAL,
}
