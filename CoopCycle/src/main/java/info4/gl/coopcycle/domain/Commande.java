package info4.gl.coopcycle.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import info4.gl.coopcycle.domain.enumeration.Etat;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Commande.
 */
@Entity
@Table(name = "commande")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@SuppressWarnings("common-java:DuplicatedBlocks")
public class Commande implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "id_commande", nullable = false, unique = true)
    private Long idCommande;

    @NotNull
    @Column(name = "id_cooperative", nullable = false, unique = true)
    private Long idCooperative;

    @NotNull
    @Column(name = "id_client", nullable = false, unique = true)
    private Long idClient;

    @NotNull
    @Column(name = "id_course", nullable = false, unique = true)
    private Long idCourse;

    @NotNull
    @Min(value = 0)
    @Column(name = "prix", nullable = false)
    private Integer prix;

    @Column(name = "date")
    private ZonedDateTime date;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "etat", nullable = false)
    private Etat etat;

    @OneToMany(mappedBy = "commande")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "products", "commande" }, allowSetters = true)
    private Set<ProduitCommande> produitCommandes = new HashSet<>();

    @JsonIgnoreProperties(value = { "commande" }, allowSetters = true)
    @OneToOne(mappedBy = "commande")
    private Paiement paiement;

    @JsonIgnoreProperties(value = { "commande", "coursier" }, allowSetters = true)
    @OneToOne(mappedBy = "commande")
    private Course course;

    @ManyToOne
    @JsonIgnoreProperties(value = { "commandes" }, allowSetters = true)
    private Client client;

    @ManyToOne
    @JsonIgnoreProperties(value = { "menus", "commandes" }, allowSetters = true)
    private Cooperative cooperative;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Commande id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getIdCommande() {
        return this.idCommande;
    }

    public Commande idCommande(Long idCommande) {
        this.setIdCommande(idCommande);
        return this;
    }

    public void setIdCommande(Long idCommande) {
        this.idCommande = idCommande;
    }

    public Long getIdCooperative() {
        return this.idCooperative;
    }

    public Commande idCooperative(Long idCooperative) {
        this.setIdCooperative(idCooperative);
        return this;
    }

    public void setIdCooperative(Long idCooperative) {
        this.idCooperative = idCooperative;
    }

    public Long getIdClient() {
        return this.idClient;
    }

    public Commande idClient(Long idClient) {
        this.setIdClient(idClient);
        return this;
    }

    public void setIdClient(Long idClient) {
        this.idClient = idClient;
    }

    public Long getIdCourse() {
        return this.idCourse;
    }

    public Commande idCourse(Long idCourse) {
        this.setIdCourse(idCourse);
        return this;
    }

    public void setIdCourse(Long idCourse) {
        this.idCourse = idCourse;
    }

    public Integer getPrix() {
        return this.prix;
    }

    public Commande prix(Integer prix) {
        this.setPrix(prix);
        return this;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public ZonedDateTime getDate() {
        return this.date;
    }

    public Commande date(ZonedDateTime date) {
        this.setDate(date);
        return this;
    }

    public void setDate(ZonedDateTime date) {
        this.date = date;
    }

    public Etat getEtat() {
        return this.etat;
    }

    public Commande etat(Etat etat) {
        this.setEtat(etat);
        return this;
    }

    public void setEtat(Etat etat) {
        this.etat = etat;
    }

    public Set<ProduitCommande> getProduitCommandes() {
        return this.produitCommandes;
    }

    public void setProduitCommandes(Set<ProduitCommande> produitCommandes) {
        if (this.produitCommandes != null) {
            this.produitCommandes.forEach(i -> i.setCommande(null));
        }
        if (produitCommandes != null) {
            produitCommandes.forEach(i -> i.setCommande(this));
        }
        this.produitCommandes = produitCommandes;
    }

    public Commande produitCommandes(Set<ProduitCommande> produitCommandes) {
        this.setProduitCommandes(produitCommandes);
        return this;
    }

    public Commande addProduitCommande(ProduitCommande produitCommande) {
        this.produitCommandes.add(produitCommande);
        produitCommande.setCommande(this);
        return this;
    }

    public Commande removeProduitCommande(ProduitCommande produitCommande) {
        this.produitCommandes.remove(produitCommande);
        produitCommande.setCommande(null);
        return this;
    }

    public Paiement getPaiement() {
        return this.paiement;
    }

    public void setPaiement(Paiement paiement) {
        if (this.paiement != null) {
            this.paiement.setCommande(null);
        }
        if (paiement != null) {
            paiement.setCommande(this);
        }
        this.paiement = paiement;
    }

    public Commande paiement(Paiement paiement) {
        this.setPaiement(paiement);
        return this;
    }

    public Course getCourse() {
        return this.course;
    }

    public void setCourse(Course course) {
        if (this.course != null) {
            this.course.setCommande(null);
        }
        if (course != null) {
            course.setCommande(this);
        }
        this.course = course;
    }

    public Commande course(Course course) {
        this.setCourse(course);
        return this;
    }

    public Client getClient() {
        return this.client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Commande client(Client client) {
        this.setClient(client);
        return this;
    }

    public Cooperative getCooperative() {
        return this.cooperative;
    }

    public void setCooperative(Cooperative cooperative) {
        this.cooperative = cooperative;
    }

    public Commande cooperative(Cooperative cooperative) {
        this.setCooperative(cooperative);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Commande)) {
            return false;
        }
        return id != null && id.equals(((Commande) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Commande{" +
            "id=" + getId() +
            ", idCommande=" + getIdCommande() +
            ", idCooperative=" + getIdCooperative() +
            ", idClient=" + getIdClient() +
            ", idCourse=" + getIdCourse() +
            ", prix=" + getPrix() +
            ", date='" + getDate() + "'" +
            ", etat='" + getEtat() + "'" +
            "}";
    }
}
