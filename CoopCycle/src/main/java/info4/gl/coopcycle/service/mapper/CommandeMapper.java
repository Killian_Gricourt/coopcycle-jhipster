package info4.gl.coopcycle.service.mapper;

import info4.gl.coopcycle.domain.Client;
import info4.gl.coopcycle.domain.Commande;
import info4.gl.coopcycle.domain.Cooperative;
import info4.gl.coopcycle.service.dto.ClientDTO;
import info4.gl.coopcycle.service.dto.CommandeDTO;
import info4.gl.coopcycle.service.dto.CooperativeDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Commande} and its DTO {@link CommandeDTO}.
 */
@Mapper(componentModel = "spring")
public interface CommandeMapper extends EntityMapper<CommandeDTO, Commande> {
    @Mapping(target = "client", source = "client", qualifiedByName = "clientId")
    @Mapping(target = "cooperative", source = "cooperative", qualifiedByName = "cooperativeId")
    CommandeDTO toDto(Commande s);

    @Named("clientId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    ClientDTO toDtoClientId(Client client);

    @Named("cooperativeId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CooperativeDTO toDtoCooperativeId(Cooperative cooperative);
}
