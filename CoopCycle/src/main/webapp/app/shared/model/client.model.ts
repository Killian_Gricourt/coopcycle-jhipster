import { ICommande } from '@/shared/model/commande.model';

export interface IClient {
  id?: number;
  nom?: string | null;
  prenom?: string | null;
  telephone?: string | null;
  adresse?: string;
  commandes?: ICommande[] | null;
}

export class Client implements IClient {
  constructor(
    public id?: number,
    public nom?: string | null,
    public prenom?: string | null,
    public telephone?: string | null,
    public adresse?: string,
    public commandes?: ICommande[] | null
  ) {}
}
