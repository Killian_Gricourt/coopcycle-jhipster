## Page 1

https://gitlab.com/Killian_Gricourt/coopcycle-jhipster

https://gitlab.com/Killian_Gricourt/coopcycle-jhipster/-/blob/main/coopcycle.jh 

https://gitlab.com/Killian_Gricourt/coopcycle-jhipster/-/blob/main/jhipster-jdl.png 

![représentation graphique du modèle coopcycle](/home/killian/Desktop/Cours/GL/COOPCYCLE/coopcycle-jhipster/coopcycle.png)

## Page 2

### Liste des commandes

```bash
mkdir CoopCycle
cd CoopCycle/
jhipster
```

### Liste des réponses aux questions

```jhipster

? Which *type* of application would you like to create? Monolithic application 
(recommended for simple projects)
? What is the base name of your application? CoopCycle
? Do you want to make it reactive with Spring WebFlux? No
? What is your default Java package name? info4.gl.coopcycle
? Which *type* of authentication would you like to use? JWT authentication 
(stateless, with a token)
? Which *type* of database would you like to use? SQL (H2, PostgreSQL, MySQL, 
MariaDB, Oracle, MSSQL)
? Which *production* database would you like to use? PostgreSQL
? Which *development* database would you like to use? H2 with disk-based 
persistence
? Which cache do you want to use? (Spring cache abstraction) Ehcache (local 
cache, for a single node)
? Do you want to use Hibernate 2nd level cache? Yes
? Would you like to use Maven or Gradle for building the backend? Maven
? Do you want to use the JHipster Registry to configure, monitor and scale your 
application? No
? Which other technologies would you like to use? 
? Which *Framework* would you like to use for the client? Vue
? Do you want to generate the admin UI? Yes
? Would you like to use a Bootswatch theme (https://bootswatch.com/)? Superhero
? Choose a Bootswatch variant navbar theme (https://bootswatch.com/)? Primary
? Would you like to enable internationalization support? Yes
? Please choose the native language of the application French
? Please choose additional languages to install Arabic (Libya), Belarusian, 
Chinese (Simplified), English, Estonian, Farsi, Ukrainian, Uzbek (Cyrillic), 
Vietnamese
? Besides JUnit and Jest, which testing frameworks would you like to use? 
Cypress, Gatling, Cucumber
? Would you like to install other generators from the JHipster Marketplace? No
? Would you like to audit Cypress tests? Yes
```

### Exemple d'entité de l'application CoopCycle

![Entité Coopérative](/home/killian/Desktop/Cours/GL/COOPCYCLE/coopcycle-jhipster/Coopérative.png)

## Page 3

### Nombre de lignes de code générées dans le répertoire ./src sans le modèle
```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
JSON                           154              0              0           8810
Java                           103           1231            606           6137
TypeScript                     100            768            260           5421
Vuejs Component                 27             86             21           2096
YAML                            18             43            295            545
HTML                            11             31             31            440
Sass                             4             37              2            423
XML                              6             33             39            237
CSS                              1              2              0            150
JavaScript                       1              1              0             54
Bourne Shell                     1              6              4             29
CSV                              3              0              0             10
Cucumber                         1              1              0              5
SVG                              4              0              0              4
Markdown                         1              0              0              1
-------------------------------------------------------------------------------
SUM:                           435           2239           1258          24362
-------------------------------------------------------------------------------
```

### Nombre de lignes de code générées dans le répertoire ./src sans le modèle et sans 'AdminUI'

```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
JSON                           114              0              0           7293
Java                           103           1231            606           6137
TypeScript                      83            606            229           4031
Vuejs Component                 21             64             21           1468
YAML                            18             43            295            545
HTML                            11             31             31            440
Sass                             4             37              2            423
XML                              6             33             39            237
CSS                              1              2              0            150
JavaScript                       1              1              0             54
Bourne Shell                     1              6              4             29
CSV                              3              0              0             10
Cucumber                         1              1              0              5
SVG                              4              0              0              4
Markdown                         1              0              0              1
-------------------------------------------------------------------------------
SUM:                           372           2055           1227          20827
-------------------------------------------------------------------------------

```

### Nombre de lignes de code générées dans le répertoire ./src avec le modèle

```
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Java                           198           2990           2101          13431
TypeScript                     192           2099            517          13391
JSON                           228              0              0          10986
Vuejs Component                 54            104             21           4727
XML                             21             73            183            734
Scala                            9             99             63            726
YAML                            18             43            295            545
HTML                            11             31             31            440
Sass                             4             37              2            423
CSS                              1              2              0            150
CSV                             12              0              0            109
JavaScript                       1              1              0             54
Bourne Shell                     1              6              4             29
Cucumber                         1              1              0              5
SVG                              4              0              0              4
Markdown                         1              0              0              1
-------------------------------------------------------------------------------
SUM:                           756           5486           3217          45755
-------------------------------------------------------------------------------

```

### Salaires

Pour un développeur full stack en france le coût mensuel est au minimum environ 3960€ (3166€ brut + 25% de charge patronale). En inde il est de 818€ (654€ + 25% de charge patronale). Aux US il est de 10 128€ (8102€ + 25% de charge patronale).

Pour développer l'application à partir de JHipster en france cela coûterait 144081\$, aux US cela coûterait 368524\$ et en Inde cela coûterait 29765\$

Pour la développer depuis zéro, cela coûterait 174732\$ en Inde, 2163334\$ aux US et 845793\$ en France.

## Page 4

![Swagger](/home/killian/Desktop/Cours/GL/COOPCYCLE/coopcycle-jhipster/Swagger.png)

## Page 5

![SonarQube](/home/killian/Desktop/Cours/GL/COOPCYCLE/coopcycle-jhipster/SonarQube.png)

## Page 6

![Heroku error 1](/home/killian/Desktop/Cours/GL/COOPCYCLE/coopcycle-jhipster/Heroku1.png)
![Heroku error 1](/home/killian/Desktop/Cours/GL/COOPCYCLE/coopcycle-jhipster/Heroku2.png)