package info4.gl.coopcycle.repository;

import info4.gl.coopcycle.domain.ProduitCommande;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;

public interface ProduitCommandeRepositoryWithBagRelationships {
    Optional<ProduitCommande> fetchBagRelationships(Optional<ProduitCommande> produitCommande);

    List<ProduitCommande> fetchBagRelationships(List<ProduitCommande> produitCommandes);

    Page<ProduitCommande> fetchBagRelationships(Page<ProduitCommande> produitCommandes);
}
