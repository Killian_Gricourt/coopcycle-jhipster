import { IMenu } from '@/shared/model/menu.model';
import { ICommande } from '@/shared/model/commande.model';

export interface ICooperative {
  id?: number;
  nom?: string | null;
  adresse?: string;
  menus?: IMenu[] | null;
  commandes?: ICommande[] | null;
}

export class Cooperative implements ICooperative {
  constructor(
    public id?: number,
    public nom?: string | null,
    public adresse?: string,
    public menus?: IMenu[] | null,
    public commandes?: ICommande[] | null
  ) {}
}
