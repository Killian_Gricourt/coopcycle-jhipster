import { ICommande } from '@/shared/model/commande.model';
import { ICoursier } from '@/shared/model/coursier.model';

export interface ICourse {
  id?: number;
  idCoursier?: number;
  idCommande?: number;
  adresse?: string;
  commande?: ICommande | null;
  coursier?: ICoursier | null;
}

export class Course implements ICourse {
  constructor(
    public id?: number,
    public idCoursier?: number,
    public idCommande?: number,
    public adresse?: string,
    public commande?: ICommande | null,
    public coursier?: ICoursier | null
  ) {}
}
