import { ICommande } from '@/shared/model/commande.model';

import { ModeDePaiement } from '@/shared/model/enumerations/mode-de-paiement.model';
export interface IPaiement {
  id?: number;
  idCommande?: number | null;
  modeDePaiement?: ModeDePaiement;
  commande?: ICommande | null;
}

export class Paiement implements IPaiement {
  constructor(
    public id?: number,
    public idCommande?: number | null,
    public modeDePaiement?: ModeDePaiement,
    public commande?: ICommande | null
  ) {}
}
