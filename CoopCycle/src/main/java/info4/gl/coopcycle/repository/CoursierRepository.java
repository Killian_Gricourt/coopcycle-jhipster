package info4.gl.coopcycle.repository;

import info4.gl.coopcycle.domain.Coursier;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data JPA repository for the Coursier entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CoursierRepository extends JpaRepository<Coursier, Long> {}
